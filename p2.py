import paramiko
import sys
import socket
import time
import re


class peer():
    def __init__(self,ip, peeras,InPkt,OutPkt, OutQ, Flaps, LastUpDown, State):
        self.ip, self.peeras,self.InPkt,self.OutPkt, self.OutQ, self.Flaps, self.LastUpDown, self.State = ip, peeras,InPkt,OutPkt, OutQ, Flaps, LastUpDown, State
    
    def __repr__(self):
        ls = [self.ip, self.peeras,self.InPkt,self.OutPkt, self.OutQ, self.Flaps, self.LastUpDown, self.State]
        # ls = [str(x) for x in ls]
        ls = (' : ').join(ls)
        return ls

class inet():
    def __init__(self, totpath, actpath, suppressed, history, dampstate, pending):
        self.totpath    = totpath
        self.actpath    = actpath
        self.suppressed = suppressed
        self.history    = history
        self.dampstate  = dampstate
        self.pending    = pending

def ssh_cli(host, username, password, cli):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname = host,username=username,password=password,timeout=120, 
        look_for_keys=False)
        connection_shell = ssh.invoke_shell()
    except paramiko.AuthenticationException:
        print("Authentication failed")
    except paramiko.SSHException:
        print("SSH error. Connection error" + host + ';'+username +';'+password)
        sys.exit('Script stopped')
    except socket.error:
        print("Socket error. Connection error")
    except socket.timeout:
        print("Socket timeout. Connection error")

    (stdin, stdout, stderr) = ssh.exec_command('cli ' + cli)
    outdata = str(stdout.read().decode("utf-8"))
    # print("Executing commands on device: "+host+"\n"+outdata)
    # Логирование
    # outdata_list = outdata.split("\n")
    # with open("./bgp_tb_log" + time.strftime("-%Y-%m-%d") + ".log", "a") as log:
    #     log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #               "  Commands executed on device: " + host + " : \n")
    #     log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #               "  " + host + "  " + cli + "\n")
    #     log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #               "  Response from device: " + host + " : \n")
    #     for line in outdata_list:
    #         print(line)
    #         log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #                   "  " + host + "  " + line + "\n")
    print(host + ' : ' + username + ' : ' + password + ' : ' + cli) 
    time.sleep(0.5)
    return outdata

class host():
    def __init__(self, ip, username, password):
        self.ip = ip
        self.username = username
        self.password = password
        self.bgp_groups = self.parse_bgp_groups()
    def find_by_peeras(self,peeras):
        answ = []   
        for group in self.bgp_groups:
            for korob in group.neigbors:
                if korob.peeras == peeras:
                    answ.append(korob)
        return answ
      
    def print_bgp_summary(self):
            return ssh_cli(self.ip, self.username, self.password, 'show bgp summary')
    #консольный дисплей бгп сессий
    def display_bgp_summary(self):
        ls = self.parse_bgp_summary()
        return '\n'.join(ls)
    def parse_bgp_summary(self):
        text = self.print_bgp_summary()
        bgp_summary_pattern1 = re.compile(r'^Groups:\s(.*)\sPeers:\s(.*)\sDown\speers:\s(.*)', re.MULTILINE)
        line1 = re.findall(bgp_summary_pattern1, text)
        self.GroupNum,self.PeersNum,self.DownPeersNum = line1[0][0],line1[0][1],line1[0][2]
        # self.PeersNum = 
        bgp_summary_pattern2 = re.compile(r'Pending\n(.*)\s*(\d*)\s*(\d*)\s*(\d*)(\d*)\s*(\d*)\s*(\d*)*\s*(\d*)',re.MULTILINE)
        line2 = re.findall(bgp_summary_pattern2,text)
        smthng = [inet(x[0],x[1],x[2],x[3],x[4],x[5]) for x in line2]
        bgp_summary_pattern3 = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s*(\d*)\s*(\d*)\s*(\d*)\s*(\d*)\s*(\d*)\s*([\d*:]*)\s(.*)\s',re.MULTILINE)
        line3 = re.findall(bgp_summary_pattern3,text)
        peers = [peer(x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]) for x in line3]
        return peers
   

    def show_group_names(self):
        if (self.ip == '192.168.1.12'):
            return ssh_cli(self.ip,self.username,self.password, 'show configuration routing-instances tb protocols bgp')
        if (self.ip == '192.168.1.2'):
            return ssh_cli(self.ip,self.username,self.password, 'show configuration protocols bgp')

    #Возвращает лист с группами имен, пример: ['RTK', 'TTK', 'BLB']
    def parse_bgp_groups_names(self):
        text = self.show_group_names()
        pattern = re.compile(r'group\s(.*)\s{',re.MULTILINE)
        ls = re.findall(pattern, text)
        print (ls)
        return ls

    def parse_bgp_groups(self):
        group_names = self.parse_bgp_groups_names()
        ls = []
        for name in group_names:
            ls.append(bgp_group(name,self.ip,self.username,self.password))
        self.bgp_groups = ls
        return ls

class bgp_group():
    def __init__(self, name, ip, username, password):
        self.name = name
        self.ip = ip
        self.username = username
        self.password = password
        self.bgp_neighbors = self.parse_bgp_neighbors()

    def get_neighbors_text(self):
        if (self.ip == '192.168.1.12'):
            text = ssh_cli(self.ip, self.username, self.password, 'show configuration routing-instances tb protocols bgp group ' + self.name + ' ')
        if (self.ip == '192.168.1.2'):
            text = ssh_cli(self.ip, self.username, self.password, 'show configuration protocols bgp group ' + self.name + ' ')
        return text
            

    #
    def parse_bgp_neighbors(self):
            text = self.get_neighbors_text()
            pattern_neighbors = r"neighbor (.*)\s{\s*description\s\"(.*)\";\s*peer-as (.*);\s}"
            neighbors = re.findall(pattern_neighbors, text)      
            self.neigbors = [bgp_neighbor(self.ip, self.name, self.password, x[0],x[1],x[2]) for x in neighbors]
            return self.neigbors

    def __repr__(self):
        return '[---  '+'bgp-group:'+self.name + '  ---]'

class bgp_neighbor():
    def __init__(self, ip, username, password, group, description, peeras):
        self.ip             = ip
        self.username       = username
        self.password       = password
        self.description    = description
        self.peeras         = peeras
        self.group          = group
        self.sessions       = self.parse_sessions()


    def get_sessions_text(self):
        return ssh_cli(self.ip,self.username,self.password, 'show configuration protocols bgp')

    def parse_sessions(self):
        text = self.get_sessions_text()
        sessions_pattern = re.compile(r'(inactive:\s|)group\s(.*)\s{\s*type\sexternal;\s*import\s(.*);\s*export\s(.*);', re.MULTILINE)
        sessions = re.findall(sessions_pattern,text)
        ls = [session('inactive' if k[0] == 'inactive: ' else 'active',k[1],k[2],k[3]) for k in sessions]
        self.sessions = ls
        return ls

    def __repr__(self):
        return self.ip+' : '+self.group+' : '+self.description+' : '+self.peeras

    def print_bgp_summary(self):
            return ssh_cli(self.ip, self.username, self.password, 'show bgp summary')
        

class session():
    def __init__(self,status, name, imp, exp):
        self.status = status
        self.name = name
        self.imp = imp
        self.exp = exp

    def __repr__(self):
        return self.name + ':'+ self.status + ':' + self.imp +':'+ self.exp

# ip = '192.168.1.2'
# username = 'root'
# pas = 'qazwsx12'
# ho = host('192.168.1.2',username,pas)
# for group in ho.bgp_groups:
#     print(group)
