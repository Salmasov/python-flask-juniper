from flask import Flask, render_template,  
                    session, redirect, url_for, escape, request
from forms import LoginForm, ChooseForm
from p2 import ssh_cli, host, peer, inet, bgp_group, bgp_neighbor

 
app = Flask(__name__)

app.config['SECRET_KEY']='1da3068225890912ad20306b8507fe3a5ae95e46'

@app.route('/login', methods = ['GET','POST'])
def login():
    form = LoginForm()
    if request.method == 'POST':
        session['username'] = request.form['username']
        session['password'] = request.form['password']
        return redirect(url_for('index'))
    return render_template('login.html',form=form)

@app.route('/logout')
def logout():
    # remove the username from the session if it is there
    session.pop('username', None)
    return redirect(url_for('index'))


def setChoices(form, bgp_groups):
    answ = []
    for group in bgp_groups:
        for korob in group.bgp_neighbors:
            answ.append((korob.peeras, korob.description))
    return answ

@app.route('/third', methods=['GET','POST'])
def third():
    if 'username' not in session:
        return redirect(url_for('login'))
        
    if 'username' in session:
        username = session['username']
        pas = session['password']
        peeras = session['peeras']
        korobk = host('192.168.1.12')
        korobki = korobk.find_by_peeras(peeras)
        desk = korobki[0].description
        myneighbor = bgp_neighbor('192.168.1.2', username, pas,'TTK', desk, peeras)


        
        return render_template('third.html', username = username, pas = pas, peeras = peeras, sum = sum,myneighbor = myneighbor)


@app.route('/', methods = ['GET','POST'])
def index():
    form = ChooseForm()
    if request.method == 'POST':
            # replace this with an insert into whatever database you're usin
            session['peeras'] = request.form.get('select')
            return redirect(url_for('third'))
    if 'username' not in session:
        return redirect(url_for('login'))

    if 'username' in session:
        username = session['username']
        pas = session['password']
        korobk = host('192.168.1.12', username,pas)
        #bgp = korobk.set_bgp_groups()

        form.select.choices = setChoices(form=form, bgp_groups=korobk.bgp_groups)
        return render_template('index.html', form=form, username=username, pas=pas)
        
    


if __name__ == '__main__':
   app.run(debug=True)