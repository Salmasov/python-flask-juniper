from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField,SelectField
from wtforms.validators import DataRequired, Length, IPAddress

class LoginForm(FlaskForm):
   """ host = StringField('IP коробки',validators=[DataRequired(),IPAddress()]) """
   username = StringField('Имя', validators=[DataRequired(), Length(min=2,max=20)])
   password = PasswordField('Пароль',validators=[DataRequired()])
   submit = SubmitField('Ввод')
   

class ChooseForm(FlaskForm):
   select = SelectField('Селект поле',validators=[])
   submit = SubmitField('Выбрать')
   

