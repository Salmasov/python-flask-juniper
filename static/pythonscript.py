import paramiko
import sys
import socket
import time
import re
import npyscreen
import curses
import json
import os




def ssh_cli(host, username, password, cli):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connection = ssh.connect(hostname = host,username=username,password=password)
    except paramiko.AuthenticationException:
        print("Authentication failedasd")
    except paramiko.SSHException:
        print("SSH error. Connection error" + host + ';'+username +';'+password)
        sys.exit('Script stopped')
    except socket.error:
        print("Socket error. Connection error")
    except socket.timeout:
        print("Socket timeout. Connection error")

    (stdin, stdout, stderr) = ssh.exec_command('cli ' + cli)
    outdata = str(stdout.read().decode("utf-8"))
    # print("Executing commands on device: "+host+"\n"+outdata)
    # Логирование
    # outdata_list = outdata.split("\n")
    # with open("./bgp_tb_log" + time.strftime("-%Y-%m-%d") + ".log", "a") as log:
    #     log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #               "  Commands executed on device: " + host + " : \n")
    #     log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #               "  " + host + "  " + cli + "\n")
    #     log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #               "  Response from device: " + host + " : \n")
    #     for line in outdata_list:
    #         print(line)
    #         log.write(time.strftime("%Y-%m-%d-%H.%M.%S") +
    #                   "  " + host + "  " + line + "\n")
    print(host +': '+ username + ': '+password + ': '+ cli)
    ssh.close()
    return outdata


def parse_neighbors(text):
    pattern_neighbors = r"neighbor (.*)\s{\s*description\s\"(.*)\";\s*peer-as (.*);\s}"
    neighbors = re.findall(pattern_neighbors, text)

    # output = map(lambda n: bgp_neighbor(x[0], x[1], x[2]), neighbors)
    # for n in neighbors:
    #     output.append(bgp_neighbor(n[0], n[1], n[2]))

    # map_output = list(map(lambda x: bgp_neighbor(x[0], x[1], x[2]), neighbors))

    comp_output = [bgp_neighbor(x[0], x[1], x[2]) for x in neighbors]
    return comp_output

# def provider_parse(providers):
#     for provider in providers:
#         parse_sessions()


# def get_neighbors_ip(group, username, password):
#     return ssh_cli(host, 'show configuration protocols bgp group ' + group)


# def aggregate_neigbors_ip(ip_list, neigbors_list):
#     return [neigbor.ip == ip for neigbor in neigbor_list for ip in ip_list]


# def parse_neighbors_ip(text):
#     neighbor_ip_pattern = re.compile(
#         r"^neighbor\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", re.MULTILINE)
#     neigbors_ip = re.findall(neighbor_ip_pattern, text)
#     return neigbors_ip

# принимает лист бгп сессий, для каждой из бгп сессий выполняет команду get_neighbors_ip


# def agregate(ls, text, neighbors_ls):
#     for neigbor in neigbors_ls:
#         get_neighbors_ip(neigbor)

#     neighbor_ip_pattern = re.compile(
#         r"^neighbor\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", re.MULTILINE)
#     neigbors_ips = re.findall(neighbor_ip_pattern, text)
#     for bgp_ses in ls:
#         neigbors_ips = get_neigbors_ip(bgp_ses)
#         bgp_ses.neighbors.append(neigbors_ip)



class inet():
    def __init__(self, totpath, actpath, suppressed, history, dampstate, pending):
        self.totpath    = totpath
        self.actpath    = actpath
        self.suppressed = suppressed
        self.history    = history
        self.dampstate  = dampstate
        self.pending    = pending

class peer():
    def __init__(self,ip, peeras,InPkt,OutPkt, OutQ, Flaps, LastUpDown, State):
        self.ip, self.peeras,self.InPkt,self.OutPkt, self.OutQ, self.Flaps, self.LastUpDown, self.State = ip, peeras,InPkt,OutPkt, OutQ, Flaps, LastUpDown, State
    
    def __repr__(self):
        ls = [self.ip, self.peeras,self.InPkt,self.OutPkt, self.OutQ, self.Flaps, self.LastUpDown, self.State]
        # ls = [str(x) for x in ls]
        ls = (' : ').join(ls)
        return ls





class host():
    def __init__(self, ip, username, password):
        self.ip             = ip
        self.username       = username
        self.password       = password
        self.bgp_groups     = self.parse_bgp_groups()
        self.bgp_sessions   = self.parse_bgp_summary()
        # self.PeersNum       = 0
        # self.GroupNum       = 0
        # self.DownPeersNum   = 0


    def find_by_peeras(self,peeras):
        answ = []   
        for group in self.bgp_groups:
            for korob in group.neigbors:
                if korob.peeras == peeras:
                    answ.append(korob)
        return answ

    def print_bgp_summary(self):
        return ssh_cli(self.ip, self.username, self.password, 'show bgp summary')
    #консольный дисплей бгп сессий
    def display_bgp_summary(self):
        ls = self.parse_bgp_summary()
        return '\n'.join(ls)
    def parse_bgp_summary(self):
        text = self.print_bgp_summary()
        bgp_summary_pattern1 = re.compile(r'^Groups:\s(.*)\sPeers:\s(.*)\sDown\speers:\s(.*)', re.MULTILINE)
        line1 = re.findall(bgp_summary_pattern1, text)
        self.GroupNum,self.PeersNum,self.DownPeersNum = line1[0][0],line1[0][1],line1[0][2]
        # self.PeersNum = 
        bgp_summary_pattern2 = re.compile(r'Pending\n(.*)\s*(\d*)\s*(\d*)\s*(\d*)(\d*)\s*(\d*)\s*(\d*)*\s*(\d*)',re.MULTILINE)
        line2 = re.findall(bgp_summary_pattern2,text)
        smthng = [inet(x[0],x[1],x[2],x[3],x[4],x[5]) for x in line2]
        bgp_summary_pattern3 = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s*(\d*)\s*(\d*)\s*(\d*)\s*(\d*)\s*(\d*)\s*([\d*:]*)\s(.*)\s',re.MULTILINE)
        line3 = re.findall(bgp_summary_pattern3,text)
        peers = [peer(x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7]) for x in line3]
        return peers



    
    # def print_neighbors(self):
    #     return ssh_cli(self.ip,self.username,self.password, 'show configuration routing-instances tb protocols bgp')

    def show_group_names(self):
        return ssh_cli(self.ip,self.username,self.password, 'show configuration routing-instances tb protocols bgp')

    #Возвращает лист с группами имен, пример: ['RTK', 'TTK', 'BLB']
    def parse_bgp_groups_names(self):
        text = self.show_group_names()
        pattern = re.compile(r'group\s(.*)\s{',re.MULTILINE)
        ls = re.findall(pattern, text)
        return ls

    def parse_bgp_groups(self):
        group_names = self.parse_bgp_groups_names()
        ls = []
        for name in group_names:
            ls.append(bgp_group(name,self.ip,self.username,self.password))
        self.bgp_groups = ls
        return ls

    def __repr__(self):
        ls = [n.__repr__() for n in self.neighbors]
        ls = '\n'.join(ls)
        return '\n\n\n'+'[---  '+'bgp-group:'+self.name + '  ---]'+'\n\n' + str(ls)

    def __repr__(self):
        ls = [n.__repr__() for n in self.bgp_groups]
        ls = '\n'.join(ls)
        return '\n\n'+'-----HOST IP-ADRESS: '+self.ip + '-----' + '\n\n' + str(ls)


# show configuration routing-instances tb protocols bgp group TTK
"""     def get_descriptions(self):
        group_names = self.parse_group_names()
        return [ self.parse_neighbors(group) for group in group_names] """


class bgp_group():
    def __init__(self, name, ip, username, password):
        self.name       = name
        self.ip         = ip
        self.username   = username
        self.password   = password
        self.neighbors  = self.parse_neighbors()

    def __len__(self):
        self.neigbors = self.parse_neighbors()
        return len(self.neigbors)

    def print_neighbors_by_group(self):
        return ssh_cli(self.ip, self.username, self.password, 'show configuration routing-instances tb protocols bgp group ' + self.name)
    
    def parse_neighbors(self):
        text = self.print_neighbors_by_group()
        pattern_neighbors = r"neighbor (.*)\s{\s*description\s\"(.*)\";\s*peer-as (.*);\s}"
        neighbors = re.findall(pattern_neighbors, text)

        # output = map(lambda n: bgp_neighbor(x[0], x[1], x[2]), neighbors)
        # for n in neighbors:
        #     output.append(bgp_neighbor(n[0], n[1], n[2]))

        # map_output = list(map(lambda x: bgp_neighbor(x[0], x[1], x[2]), neighbors))
        self.neigbors = [bgp_neighbor(self.ip,self.name,self.password, x[0],x[1],x[2]) for x in neighbors]
        return self.neigbors

    def __repr__(self):
        ls = [n.__repr__() for n in self.neighbors]
        ls = '\n'.join(ls)
        return '\n\n\n'+'[---  '+'bgp-group:'+self.name + '  ---]'+'\n\n' + str(ls)


class bgp_neighbor(host):
    def __init__(self, ip, username, password, group, description, peeras):
        super().__init__(ip,username,password)
        self.description    = description
        self.peeras         = peeras
        self.group          = group
        self.bgp_groups     = self.parse_bgp_groups()
        self.bgp_sessions   = self.parse_bgp_summary()

    
    #Переопределён
    def print_bgp_summary(self):
        return ssh_cli('192.168.1.2', self.username, self.password, 'show bgp summary')
    #Переопределен
    def parse_bgp_groups(self):
        text = self.show_group_names()
        group_name_pattern = re.compile(
            r'(inactive:\s|)group\s(.*)\s{\s*type\sexternal;\s*import\s(.*);\s*export\s(.*);', re.MULTILINE)
        pat = re.findall(group_name_pattern, text)
        groups = [bgp_group('inactive' if p[0]=='inactive: ' else 'active',p[1],[2],p[3],self.ip,self.username,self.password) for p in pat]
        return groups
   
    def __repr__(self):
        return self.ip+' : '+self.group+' : '+self.description+' : '+self.peeras





# host = '192.168.1.2'
# sessions = ssh_cli(host, "show configuration protocols bgp")
# host3 = host('192.168.1.12')
    # k = host('192.168.1.12')
    # k.set_bgp_groups()
    # print(ssh_cli(k.ip,'show bgp summary'))
    # print(k)
    # x = input ("Выберите peer-as коробки: ")
    # korob = select_korobka(x,host)
# korob = host('192.168.1.2')
# print(korob.parse_bgp_summary())
    

    # print(ssh_cli(korob.ip,'show bgp summary'))

    
    # print(aggregate_neigbors_ip(parse_neighbors_ip(
    #     get_neighbors_ip('BLB')), parse_neigbors('BLB')))
    # print(parse_sessions(sessions))
    # print(get_neighbors_ip("BLB"))
    # print(ssh_cli(host, "show bgp summary"))
    # show configuration protocols bgp
    # neighbors = ssh_cli(host2, 'show configuration routing-instances ' + 'tb' +
    #                     ' protocols bgp group ' + 'BLB')
    # print(parse_neighbors(neighbors))
    # print(ssh_cli( host,'show configuration'))
# sys.exit(0)
